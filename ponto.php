<?php
session_start();

require_once("inc/config_database.php");

//pega o valor de ponto_id passado na url via GET
$ponto_id = $_GET['ponto_id'];
$nome	= $_GET['nome'];
$user_id = $_SESSION['user_id'];

$query1 = 'SELECT COUNT(id) AS total FROM nota WHERE ponto_id="'.$ponto_id.'"';
$result1 = mysql_query($query1);
while ($row1 = mysql_fetch_assoc($result1)) {
	$total_votos = $row1['total'];
}

$query2 = 'SELECT nota FROM nota WHERE ponto_id="'.$ponto_id.'" AND user_id='.$user_id;
$result2 = mysql_query($query2);
$sua_nota = 0;
while ($row2 = mysql_fetch_assoc($result2)) {
	$sua_nota = $row2['nota'];
}

//pega uma media das notas do ponto selecionado
$query = 'SELECT AVG(nota) AS notaa FROM nota WHERE ponto_id="'.$ponto_id.'"';
$result = mysql_query($query);
if (!$result) {
    die('Invalid query: ' . mysql_error());
}
else
{
	//percorre os resultados
	while ($row = mysql_fetch_assoc($result)) {
		//como so esperamos um resultado, armazenamos numa variavel nota
		$nota = $row['notaa'];
	}
}
?>

<html>
	<head>
		<script src="js/jquery.min.js"></script>
		<script type="text/javascript">
			function selectEstrela(id,nota)
			{
				x = 1;
				while(x <= nota)
				{
					$("#estrela_"+id+"_"+x).attr("class","cmt-rate ok");
					x++;
				}
				while(x <= 5)
				{
					$("#estrela_"+id+"_"+x).attr("class","cmt-rate");
					x++;
				}

			}
			function SaveVoto(id,nota)
			{
				<?php if($user_id > 0){ ?>
					$.ajax({
						type: "POST",
						url: "salvar_nota.php?ponto_id="+id+"&nota="+nota,
						data: '',
						success: function(data){
							selectEstrela(id,nota);
							mostraVoto(id,nota);
							location.reload();
						}
					});
				<?php }else{ ?>
					alert("Desculpe. Voce precisa logar para poder votar");
				<?php } ?>
			}
			function mostraVoto(id,nota)
			{
				for(i=1;i<=5;i++)
				{
					$("#estrela_"+i).attr("href","javascript:void(0);");
					$("#estrela_"+i).removeAttr("onmouseover");
					if(i > nota)
					{
						$("#estrela_"+i).attr("class","cmt-rate-off");
					}
				}
			}

		</script>
		<style type="text/css">
			body{
				padding: 0;
				margin: 0;
				font-family: arial, verdana, tahoma;
			}
			.cmt-rate  {float:left; width:16px; height:16px; background:url(img/star-ico.png) no-repeat; margin:0 2px;}
			a.cmt-rate-off  {float:left; width:16px; height:16px; background:url(img/star-ico.png) no-repeat; margin:0 2px;}
			a.cmt-rate:hover, a.cmt-rate.ok  {background-position:bottom;}
		</style>
	</head>
	<body>
		<p><?php echo $nome; ?></p>
		<p>
		<?php
		if($nota > 0)
			$nota = round($nota);
		else
			$nota = 0;

		for($l=1;$l<=5;$l++)
		{
			if($sua_nota <= 0) {
				if ($l <= $nota) {
					?>
					<a href="javascript:SaveVoto('<?php echo $ponto_id; ?>',<?= $l; ?>);" id="estrela_<?= $l; ?>"
					   class="cmt-rate ok" onmouseover="selectEstrela('<?php echo $ponto_id; ?>',<?php echo $l;?>)"></a>
				<?php
				} else {
					?>
					<a href="javascript:SaveVoto('<?php echo $ponto_id; ?>',<?= $l; ?>);" id="estrela_<?= $l; ?>"
					   class="cmt-rate" onmouseover="selectEstrela('<?php echo $ponto_id; ?>',<?php echo $l;?>)"></a>
				<?php
				}
			}else{
				if ($l <= $nota) {
					?>
					<a id="estrela_<?= $l; ?>" class="cmt-rate ok"></a>
				<?php
				} else {
					?>
					<a id="estrela_<?= $l; ?>" class="cmt-rate"></a>
				<?php
				}
			}
		}
		?>
		</p>
		<br/>
		<p>Votos: <?php echo $total_votos; ?></p>
		<?php
		if($sua_nota > 0) { ?>
			<p>Sua Nota: <?php echo $sua_nota; ?></p>
		<?php
		}
		?>

	</body>
</html>