<?php
session_start();

require_once realpath(dirname(__FILE__) . '/google-api-php/autoload.php');
require_once ('cadastra_usuario.php');

$client_id = '587511262832-cnrp0t9dhe86ug0r62qi517qf59d1s34.apps.googleusercontent.com';
$client_secret = 'rGiK3JGdhxaMWAupdamjenwX';
$redirect_uri = 'http://tesi.grupomundi.com.br/login.php';

$client = new Google_Client();
$client->setClientId($client_id);
$client->setClientSecret($client_secret);
$client->setRedirectUri($redirect_uri);
$client->setScopes('email');

/************************************************
If we're logging out we just need to clear our
local access token in this case
 ************************************************/
if (isset($_REQUEST['logout'])) {
    unset($_SESSION['access_token']);
    unset($_SESSION['user_id']);
    unset($_SESSION['logado']);
}

/************************************************
If we have a code back from the OAuth 2.0 flow,
we need to exchange that with the authenticate()
function. We store the resultant access token
bundle in the session, and redirect to ourself.
 ************************************************/
if (isset($_GET['code'])) {
    $client->authenticate($_GET['code']);
    $_SESSION['access_token'] = $client->getAccessToken();
    $redirect = 'http://' . $_SERVER['HTTP_HOST'] . $_SERVER['PHP_SELF'];
    header('Location: ' . filter_var($redirect, FILTER_SANITIZE_URL));
}

/************************************************
If we have an access token, we can make
requests, else we generate an authentication URL.
 ************************************************/
if (isset($_SESSION['access_token']) && $_SESSION['access_token']) {
    $client->setAccessToken($_SESSION['access_token']);
} else {
    $authUrl = $client->createAuthUrl();
}

/************************************************
If we're signed in we can go ahead and retrieve
the ID token, which is part of the bundle of
data that is exchange in the authenticate step
- we only need to do a network call if we have
to retrieve the Google certificate to verify it,
and that can be cached.
 ************************************************/
if ($client->getAccessToken()) {
    $_SESSION['access_token'] = $client->getAccessToken();
    $token_data = $client->verifyIdToken()->getAttributes();
}


if(isset($authUrl)){
    if(!isset($_REQUEST['logout'])){
        header('location: ' . $authUrl);
    }
    else{
        header('location: index.php');
    }
}
else{
    $_SESSION['logado'] = true;
    $_SESSION['email'] = $token_data['payload']['email'];
    if(checaUsuarioJaCadastrado($_SESSION['email']) <= 0){
        cadastraUsuario($_SESSION['email']);
    }
    $_SESSION['user_id'] = getIdUserByEmail($_SESSION['email']);
    header('location: index.php');
}
?>