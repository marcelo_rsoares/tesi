<?php
session_start();
?>
<!DOCTYPE html>
<html lang="pt-br">
    <head>
        <meta charset="utf-8" />
        <title>Sistema de Avaliação de Acessebilidade para cadeirantes</title>
        <link rel="stylesheet" type="text/css" href="css/estilo.css">
        <style type="text/css">
            input{
                font-family: arial, verdana, tahoma;
                font-size: 18px;
                min-height: 45px;
            }
            body{
                padding: 0;
                margin: 0;
                font-family: arial, verdana, tahoma;
            }
            #select_cidade {
                background-color: #f0f0f0;
                font-size: 30px;
                border: 1px solid rgb(240, 240, 240);
                text-align: center;
                padding: 20px;
            }
            #userbar{
                padding: 1px;
                margin: 0;
                text-align: right;
                background-color: beige;
            }
        </style>
    </head>
 
    <body>
        <?php include("inc/userbar.php"); ?>
        <div id="select_cidade">
            <p>Por favor, digite o nome da cidade que você está:</p>
            <input type="text" name="cidade" id="cidade" value="Teresina" />
            <input type="button" name="searchBtn" value="Selecionar" onclick="javascript:selectCity();" />
            <p>Ou através da sua posição atual:</p>
            <input type="button" name="searchBtn" value="Minha posição" onclick="javascript:showByMyPosition();" />
        </div>
    	<div id="mapa" style="height: 600px; width: 100%; display:none"></div>
		<script src="js/jquery.min.js"></script>
        <script src="https://maps.googleapis.com/maps/api/js?v=3.exp&libraries=places"></script>
        <!-- aqui ficam os codigos do mapa -->
		<script src="js/mapa.js"></script>
    </body>
</html>