var map;
var service;
var infowindow;

function initialize() {
  // var center = new google.maps.LatLng(-5.070473, -42.831331);

  // var defaultBounds = new google.maps.LatLngBounds(
  // new google.maps.LatLng(-5.070473, -42.831331),
  // new google.maps.LatLng(-5.154000, -42.659132));

  map = new google.maps.Map(document.getElementById('mapa'), {
    center: new google.maps.LatLng(-5.070473, -42.831331),
    zoom: 15
  });

    // Listen for the dragend event
  google.maps.event.addListener(map, 'dragend', function() {
    if (strictBounds.contains(map.getCenter())) return;

    // We're out of bounds - Move the map back within the bounds
    var c = map.getCenter(),
    x = c.lng(),
    y = c.lat(),
    maxX = strictBounds.getNorthEast().lng(),
    maxY = strictBounds.getNorthEast().lat(),
    minX = strictBounds.getSouthWest().lng(),
    minY = strictBounds.getSouthWest().lat();

    if (x < minX) x = minX;
    if (x > maxX) x = maxX;
    if (y < minY) y = minY;
    if (y > maxY) y = maxY;

    map.setCenter(new google.maps.LatLng(y, x));
  });

  // map = new google.maps.Map(document.getElementById('mapa'), {
  //   center: new google.maps.LatLng(-5.070473, -42.831331),
  //   zoom: 15
  // });

  // var request = {
  //   // bounds: defaultBounds,
  //   location: center,
  //   radius: 1000,
  //   types: ['store']
  // };

  infowindow = new google.maps.InfoWindow();
  var service = new google.maps.places.PlacesService(map);
  service.search(request, callback);


}

function callback(results, status) {
  if (status == google.maps.places.PlacesServiceStatus.OK) {
    for (var i = 0; i < results.length; i++) {
      createMarker(results[i]);
    }
  }
}

function createMarker(place) {
  // var placeLoc = place.geometry.location;
  var marker = new google.maps.Marker({
    map: map,
    position: place.geometry.location
  });

  google.maps.event.addListener(marker, 'click', function() {

    infowindow.setContent("Id: "+place.id+" Nome: "+place.name);
    infowindow.open(map, this);
  });
}

google.maps.event.addDomListener(window, 'load', initialize);