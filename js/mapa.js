var map;
var infowindow;
var center;
var service;


//metodo que é chamado quando realiza a busca dos pontos
function callback(results, status) {
	//se o resultado for positivo
  if (status == google.maps.places.PlacesServiceStatus.OK) {
	  //percorre todos os pontos
    for (var i = 0; i < results.length; i++) {
		//chama o metodo que cria o marcador
      var place = results[i];
      createMarker(results[i]);
    }
  }
}

function checkBounds(){

    var request = {
	    location: map.getCenter(),
		types: ['school'],
	    radius: 1000,
	};

	//faz uma busca pelos pontos (bounds) no Google Places, pegando o centro atual definido, que foi atualizado
	service = new google.maps.places.PlacesService(map);
	service.search(request, callback);
}

//metodo que preenche o conteudo da caixinha quando clica em cima do marcador
function load_content(nome,id){
	//preenche com um iframe chamando a pagina ponto.php
	return "<iframe width='250px' height='200px' frameborder='0' src='ponto.php?ponto_id="+id+"&nome="+nome+"'></iframe>";
}

//metodo que cria o marcador em cima dos pontos
function createMarker(place) {

	//inicializa a caixa de dialogo quando clica no ponto
	infowindow = new google.maps.InfoWindow();

	//criar um marcador com base no ponto que recebeu como parametro no metodo
	var marker = new google.maps.Marker({
		map: map,
		position: place.geometry.location,
		icon: 'img/marker.png'
	});

	//quando executa um clique chama esse metodo
	google.maps.event.addListener(marker, 'click', function() {

		//chama o metodo que pega o conteudo, preenche o conteudo e exibe a caixinha
		content = load_content(place.name,place.id);
		infowindow.setContent(content);
	    infowindow.open(map, this);
	});
}

function initialize(lat,lng){

	$("#mapa").show();

	// chamar o método initialize quando carregar a página
	//google.maps.event.addDomListener(window, 'load', initialize);

	//seta o centro do mapa como um ponto que defini dentro de Teresina

	center = new google.maps.LatLng(lat, lng);
	//center = new google.maps.LatLng(-5.070473, -42.831331);

	//gera o mapa e imprime dentro da div mapa
	map = new google.maps.Map(document.getElementById('mapa'), {
		center: center,
		zoom: 15
	});

	var request = {
		location: center,
		types: ['school'],
		radius: 1000,
	};

	//faz uma busca pelos pontos (bounds) no Google Places, pegando o centro inicial
	service = new google.maps.places.PlacesService(map);
	service.search(request, callback);

// chama o método checkBounds sempre que o zoom for alterado
	google.maps.event.addListener(map, 'zoom_changed', function() {
		checkBounds();
	});

// chama o método checkBounds sempre que o centro do mapa for alterado (o mapa for movido)
	google.maps.event.addListener(map, 'center_changed', function() {
		checkBounds();
	});

}


function selectCity(){
	getInfo = $.get( "http://maps.googleapis.com/maps/api/geocode/json?address="+$("#cidade").val()+"&sensor=false", function( data ) {
		if(data.status == 'OK'){
			lat = data.results[0].geometry.location['lat'];
			lng = data.results[0].geometry.location['lng'];
			initialize(lat,lng);
			$("#select_cidade").hide();
		}
		else{
			alert("Cidade nao encontrada. Por favor, tente novamente");
		}
	});
}

function showByMyPosition(){
	navigator.geolocation.getCurrentPosition(
		function(position) {
			initialize(position.coords.latitude,position.coords.longitude);
		},
		function() {
			alert("Desculpe. O seu navegador nao suporta essa funcionalidade")
		}
	);

	$("#select_cidade").hide();
}