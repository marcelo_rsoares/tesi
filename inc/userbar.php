<?php if(isset($_SESSION['logado'])){ ?>
    <div id="userbar">
        <p>Você está logado como <?php echo $_SESSION['email']; ?> (<a href="login.php?logout=true">Sair</a>)</p>
    </div>
<?php
}else{
?>
    <div id="userbar">
        <p>Pra você poder votar precisa estar logado, <input type="button" value="fazer login com google" onclick="window.location='login.php'" /></p>
    </div>
<?php
}
?>